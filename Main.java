public class Main {

    // Use two loop
    public static void printLeadersTwoLoop(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            int j;
            for (j = i + 1; j < n; j++) {
                if (arr[i] <= arr[j])
                    break;
            }
            if (j == n)
                System.out.print(arr[i] + " ");
        }

    }

    // scan from right
    public static void printLeaderScanFromRight(int[] arr, int n) {
        int leader = arr[n - 1];
        // The last elememt is always leader
        System.out.print(leader + " ");

        for (int i = n - 1; i >= 0; i--) {
            if (arr[i] > leader) {
                System.out.print(arr[i] + " ");
                leader = arr[i];
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {5, 10, 6, 7, 4, 5, 2};
        printLeadersTwoLoop(arr, arr.length);
        System.out.println("====================================");
        printLeaderScanFromRight(arr, arr.length);
    }
}
